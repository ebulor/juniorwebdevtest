<?php
include_once dirname(__FILE__).'/../class/dbh.classes.php';
class TestClass extends Dbh{
    public function insert()  
    {  
         $sql = "INSERT INTO product (sku,name,price, weight, size, length, height, width) 
         VALUES ('" . $this->getSku() . "','" . $this->getName() . "','" . $this->getPrice() . "','" . $this->getWeight() . "','" . $this->getSize() . "','" . $this->getHeight() . "','" . $this->getLength() . "','" . $this->getWidth() . "')";
         $result=mysqli_query($this->conn, $sql);  
         return $result; 
    }

    public function displayProducts(){
        $sql = "SELECT * FROM product";       
        $result = mysqli_query($this->conn, $sql);
        
        while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
              
              
              echo'
              <div class="display-group">';
              echo'
              <div class="checkbox">';
               echo '<input value="'.$row['product_id'].'" type="checkbox" name="id[]" class="delete-checkbox"> ';                   
               echo '</div>';   
               echo '<p>'.$row['sku'] .'</p>';
                echo '<p>'.$row['name'] .'</p>';
               echo '<p>'.$row['price'] ."$".'</p>';
               if (!empty($row["size"])){echo "<p>Size: ".$row['size']." CM"."</p>";}
               if (!empty($row["weight"])){echo "<p>Weight: ".$row['weight']." KG"."</p>";}
               if (!empty($row["height"])&& !empty($row["width"])&& !empty($row["length"])){
                   echo "<p>Dimension: ". $row['height'] ."x". $row['width'] ."x". $row['length'] ."</p>"
                ;}           
                echo '</div>';              
            }
            $result -> free_result();
         
    }

    public function deleteProducts(){
        if(isset($_POST["id"])){      
            foreach($_POST["id"] as $product_id){              
               $sql = "DELETE FROM product WHERE product_id = '$product_id'";
               mysqli_query($this->conn, $sql);
            }
        }  
        
    } 
    
    
}
