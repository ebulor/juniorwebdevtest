<?php
  include_once ('includes/test.inc.php');  
  include_once ('class/test.classes.php');  
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/main.css" />
    <title>Junior web development test</title>
  </head>
  <body>
    <main>
      <form action="index.php" method="POST">
        <div class="header">
          <h1>Product List</h1>
          <div class="btns">
              <a class="add-btn" href="add-product.php">ADD</a>            
              <input type="submit" id="delete-product-btn" name="delete" value="MASS DELETE">
          </div>
        </div>                   
          <div class="display">                   
              <?php                                          
                $obj = new TestClass();
                $obj->displayProducts();                                                   
              ?>          
          </div>
      </form>
    </main>
  <footer>
    <p>Scandiweb Test assignment</p>
  </footer>
    
  </body>
</html>
